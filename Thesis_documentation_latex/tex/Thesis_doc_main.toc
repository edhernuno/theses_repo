\contentsline {chapter}{List of Figures}{iii}
\contentsline {chapter}{List of Tables}{v}
\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {1.1}Problem Description}{3}
\contentsline {section}{\numberline {1.2}Goal}{4}
\contentsline {section}{\numberline {1.3}Justification}{4}
\contentsline {section}{\numberline {1.4}Delimitations}{4}
\contentsline {chapter}{\numberline {2}Fundamentals}{5}
\contentsline {section}{\numberline {2.1}Computational linguistics}{5}
\contentsline {section}{\numberline {2.2}Machine translation approaches}{5}
\contentsline {subsection}{\numberline {2.2.1}Rule based}{5}
\contentsline {subsection}{\numberline {2.2.2}\Gls {corp} based}{5}
\contentsline {subsection}{\numberline {2.2.3}Interlingua}{6}
\contentsline {subsection}{\numberline {2.2.4}Statistical}{6}
\contentsline {subsection}{\numberline {2.2.5}Neural Translation}{6}
\contentsline {section}{\numberline {2.3}In deepth look at \acrshort {nmt}}{7}
\contentsline {subsection}{\numberline {2.3.1}Artificial Neural Networks}{7}
\contentsline {subsubsection}{Neurons}{7}
\contentsline {subsubsection}{Perceptron}{7}
\contentsline {subsubsection}{Sigmoid, Tanh and ReLU Neurons}{7}
\contentsline {subsection}{\numberline {2.3.2}Training \acrshort {ann}}{7}
\contentsline {subsubsection}{Gradiant descent}{7}
\contentsline {subsubsection}{Backpropagation}{7}
\contentsline {subsubsection}{Optimizers and hiper-parameters}{7}
\contentsline {subsection}{\numberline {2.3.3}CNN}{7}
\contentsline {subsection}{\numberline {2.3.4}RNN}{7}
\contentsline {subsubsection}{LSTM}{7}
\contentsline {subsection}{\numberline {2.3.5}\acrshort {nmt} Datasets}{7}
\contentsline {subsubsection}{Tokenization and Detokenization of data}{7}
\contentsline {subsection}{\numberline {2.3.6}Evaluaci\'on de traducci\'on autom\'atica}{8}
\contentsline {section}{\numberline {2.4}Antecedentes}{8}
\contentsline {subsection}{\numberline {2.4.1}Procesamiento de Lenguaje Natural basado en \textit {Deep Learning}, Tendencias Recientes }{8}
\contentsline {subsection}{\numberline {2.4.2}Desempe\~no de algunos modelos de \textit {Deep Learning} en Traducci\'on Autom\'atica (ingl\'es$\rightarrow $frances)}{9}
\contentsline {subsection}{\numberline {2.4.3}Normalizaci\'on ortogr\'afica de japon\'es para SMT}{9}
\contentsline {chapter}{Bibliography}{15}
